const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'production';
const path = require('path');

module.exports = {
	entry: './src/index.js',
	resolve: {
		alias: {
			svelte: path.dirname(require.resolve('svelte/package.json'))
		},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	module: {
	  rules: [
		{
			test: /\.svelte$/,
			use: {
				loader: 'svelte-loader',
				options: {
					compilerOptions: {
						dev: !prod
					},
					emitCss: prod,
					hotReload: !prod
				}
			}
		},
		{
			// required to prevent errors from Svelte on Webpack 5+
			test: /node_modules\/svelte\/.*\.mjs$/,
			resolve: {
				fullySpecified: false
			}
		},
		{
			test: /\.js$/,
			exclude: /node_modules/,
			use: [
			  {
				loader: 'babel-loader',
				options: {
				  presets: [ '@babel/preset-env' ],
				  plugins: [
						[
							// "@babel/plugin-syntax-class-properties",
							"@babel/plugin-proposal-class-properties",
						]
				  ]
				},
			  },
			]
		  },
		{
		  test: /\.css$/,
		  use: [
			'style-loader',
			'css-loader'
		  ]
		},
		{
			test: /\.s[ac]ss$/i,
			use: [
			  'style-loader',
			  'css-loader',
			  'sass-loader',
			],
		  },
		{
			test: /\.(svg)$/,
			use: [
			  {
				loader: 'raw-loader',
			  }
			]
		  }
	  ]
	},
	output: {
	  path: __dirname + '/dist',
	  publicPath: '/',
	  filename: 'bundle.js',
	  library: 'CustomBlock',
	  libraryTarget: 'umd'
	}
  };