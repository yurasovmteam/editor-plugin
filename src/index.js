require('./style.scss').toString();
const { Block } = require( '@editorjs-custom-blocks/form-builder' );

module.exports	= class CustomBlock {
	constructor({ data, api, config }){
		this.data = data;
		this.api = api;
		this.config = config;
		this.block;
	}

	render(){
		$layout = document.createElement( 'div' );

		this.block = new Block({
			target: $layout,
			props: {
				data : this.data,
				api : this.api,
				config : this.config,
			}
		});

		return $layout;
	}

	async save( $layout ) {
		return this.block.getData();
	}

	validate(savedData){
		return true;
	}

	static get toolbox() {
		return {
			icon	: require('./toolbox-icon.svg').default,
		};
	}
}